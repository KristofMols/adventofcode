local main_file = require 'mainLogic'
local argparse = require "lib.argparse"

local parser = argparse()
parser:argument("mode"):choices({'run', 'generate'}):args(1)
parser:option("--model"):name('-m'):count('0-1')
parser:option("--year"):name('-y'):choices({'2015', '2016', '2017', '2018', '2019', '2020', '2021', '2022'}):count('0-1')
parser:option("--day"):name('-d'):choices({'01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25'}):count('0-1')

local args = parser:parse()
local model = args['model']
local year = args['year']
local day  = args['day']
local mode = args['mode']

main_file:new(mode, year, day, model):run()