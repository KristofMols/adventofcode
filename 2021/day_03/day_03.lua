local DiagnosticReport = require '2021.models.DiagnosticReport'

local day_03 = require('lib.Class'):extend()

function day_03:init()
    self.lines   = lines_from('2021/day_03/day_03-input.txt')
    self.title   = 'Binary Diagnostic'
    self.content = {}

    M.each(self.lines, function(val, idx)
        M.push(self.content, DiagnosticReport:new(val))
    end)
end

function day_03:part_1()
    local gamma   = ''
    local epsilon = ''

    for i = 1, M.nth(self.content, 1):get_length() do
        local values = M.chain(self.content)
            :map(function(val) return val:get_bit(i) end)
            :flatten()
            :countBy(string.lower)
            :value()
        
        local zero_val = values['0']
        local one_val  = values['1']

        gamma   = gamma   .. (zero_val > one_val and '0' or '1')
        epsilon = epsilon .. (zero_val < one_val and '0' or '1')
    end
    
    return tonumber(gamma, 2) * tonumber(epsilon, 2)
end

function day_03:part_2()
    local oxygen = 0
    local co2    = 0
    local filtered_content = self.content
    
    for i = 1, M.nth(self.content, 1):get_length() do
        local values = M.chain(filtered_content)
            :map(function(val) return val:get_bit(i) end)
            :flatten()
            :countBy(string.lower)
            :value()
        
        local zero_val = values['0'] or 0
        local one_val  = values['1'] or 0

        filtered_content = M.select(filtered_content, function(val) return val:get_bit(i) == ((zero_val < one_val or zero_val == one_val) and '1' or '0') end)

        if M.size(filtered_content) == 1 then
            oxygen = tonumber(M.nth(filtered_content, 1):get_bit_string(), 2)
        end
    end

    local filtered_content = self.content

    for i = 1, M.nth(self.content, 1):get_length() do
        local values = M.chain(filtered_content)
            :map(function(val) return val:get_bit(i) end)
            :flatten()
            :countBy(string.lower)
            :value()
        
        local zero_val = values['0'] or 0
        local one_val  = values['1'] or 0

        filtered_content = M.select(filtered_content, function(val) return val:get_bit(i) == ((one_val > zero_val or zero_val == one_val) and '0' or '1') end)
        
        if M.size(filtered_content) == 1 then
            co2 = tonumber(M.nth(filtered_content, 1):get_bit_string(), 2)
            break
        end
    end

    return oxygen * co2
end

return day_03
