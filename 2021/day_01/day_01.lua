local day_01 = require('lib.Class'):extend()

function day_01:init()
    self.lines   = lines_from('2021/day_01/day_01-input.txt')
    self.title   = 'Sonar Sweep'
    self.content = {}

    M.each(self.lines, function(val, idx)
        M.push(self.content, tonumber(val))
    end)
end

function day_01:part_1()
    local result       = 0
    local previous_val = 0

    M.each(self.content, function(val, idx)
        if idx == 1 then
            previous_val = val
        else
            if val > previous_val then result = result + 1 end

            previous_val = val
        end
    end)

    return result
end

function day_01:part_2()
    local result       = 0
    local previous_sum = M.chain(self.content):slice(1, 3):sum():value()
    
    M.each(self.content, function(val, idx)
        if idx > 3 then
            local current_sum = M.chain(self.content):slice(idx - 2, idx):sum():value()

            if (current_sum > previous_sum) then result = result + 1 end

            previous_sum = current_sum
        end
    end)

    return result
end

return day_01
