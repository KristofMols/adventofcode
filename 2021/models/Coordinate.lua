local Coordinate = require('lib.Class'):extend()

function Coordinate:init(coord_input)
    self.x1, self.y1, self.x2, self.y2 = string.match(coord_input, "(%d+),(%d+) %-> (%d+),(%d+)")
end

function Coordinate:get_coord_list(include_diagonal)
    local list = {}

    if self.x1 == self.x2 then
        for idx = 0, math.abs(self.y1 - self.y2) do
            M.push(list, self.x1 .. "|" .. math.min(self.y1, self.y2) + idx)
        end
    elseif self.y1 == self.y2 then
        for idx = 0, math.abs(self.x1 - self.x2) do
            M.push(list, math.min(self.x1, self.x2) + idx .. "|" .. self.y1)
        end
    elseif include_diagonal then
        -- print(self.x1, self.y1, self.x2, self.y2, self.x1 == self.x2, self.y1 == self.y2)
    end

    return list
end

return Coordinate
