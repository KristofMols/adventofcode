local Direction = require('lib.Class'):extend()

function Direction:init(dir, count)
    self.direction = dir
    self.amount    = count
end

function Direction:get_direction() return self.direction end

function Direction:get_amount() return self.amount end

function Direction:get_horizontal_count()
    if self.direction == "forward" then 
        return self.amount 
    else 
        return 0 
    end
end

function Direction:get_vertical_count()
    if self.direction == "up" then 
        return -self.amount 
    elseif self.direction == "down" then 
        return self.amount 
    else 
        return 0 
    end
end

return Direction