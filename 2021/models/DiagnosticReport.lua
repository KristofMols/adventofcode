local DiagnosticReport = require('lib.Class'):extend()

function DiagnosticReport:init(new_bits)
    self.bit_string = new_bits
    self.bits       = {}

    for w in new_bits:gmatch('.') do self.bits[#self.bits+1] = w end
end

function DiagnosticReport:get_bit(n)
    return self.bits[n]
end

function DiagnosticReport:get_bits()
    return self.bits
end

function DiagnosticReport:get_bit_string()
    return self.bit_string
end

function DiagnosticReport:get_length()
    return M.size(self.bits)
end

return DiagnosticReport
