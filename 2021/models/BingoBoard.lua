local BingoBoard = require('lib.Class'):extend()

function BingoBoard:init()
    self.nums     = {}
    self.rows     = {}
    self.cols     = {}
    self.complete = false
    self.last_num = nil
    
    M.times(function(idx)
        self.rows[idx] = {}
        self.cols[idx] = {}
    end, 5)
end

function BingoBoard:add_row(row_init, row_num)
    local idx = 1
    for current_char in row_init:gmatch("%w+") do
        M.push(self.rows[row_num], current_char)
        M.push(self.cols[idx], current_char)

        idx = idx + 1
    end
end

function BingoBoard:set_draws(draws)
    self.nums = draws
end

function BingoBoard:is_complete()
    return self.complete
end

function BingoBoard:set_complete()
    self.complete = true
end

function BingoBoard:add_draw(num)
    if self:is_complete() then 
        return nil 
    else
        M.push(self.nums, num)

        self.last_num = num

        return self:check_win()
    end
end

function BingoBoard:check_full(num_list)
    return M.chain(num_list)
                :select(function(item) return M.detect(self.nums, item) ~= nil end)
                :count()
                :value() == 5
end

function BingoBoard:get_unmarked()
    return M.chain(self.rows)
                :flatten()
                :select(function(item) return M.detect(self.nums, item) == nil end)
                :sum()
                :value()
end

function BingoBoard:check_win()
    local res = nil

    M.each(self.rows, function(row)
        if res == nil and self:check_full(row) then
            res = self:get_unmarked() * self.last_num
        end
    end)

    if res == nil then
        M.each(self.cols, function(row)
            if res == nil and self:check_full(row) then
                res = self:get_unmarked() * self.last_num
            end
        end)
    end

    return res
end

return BingoBoard
