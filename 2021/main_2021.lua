require 'common.util'

local year_2021 = require('lib.Class'):extend()

function year_2021:init()
end

function year_2021:print_splash_screen()
    color_print({
        '                   ~  ~ ~ ~~ ~~~~~~~~~~~~~~~~~~~~  ',
        '%{bright white}  1',
        '%{bright yellow} **\n'
    })
    color_print({
        '                                     .     ..\'\'\'\'  ',
        '%{bright white}  2',
        '%{bright yellow} **\n'
    })
    color_print({
        '                                      .   :        ',
        '%{bright white}  2',
        '%{bright yellow} **\n'
    })
    color_print({
        '                          \'  .  .\'    ....\'        ',
        '%{bright white}  2',
        '%{bright yellow} **\n'
    })
    color_print({
        '     .     .   . .~ .         ..|\\..\'\'             ',
        '%{bright white}  2',
        '%{bright red} **\n'
    })
    color_print({
        '        \'.  .        .  ..   :                     ',
        '%{bright white}  2',
        '%{bright white} **\n'
    })
    color_print({
        '       .       .           :\'                      ',
        '%{bright white}  2',
        '%{bright white} **\n'
    })
    color_print({
        '       \' ...  ~       .. .  \'\'\'\'\'.....             ',
        '%{bright white}  2',
        '%{bright white} **\n'
    })
    io.write('\n')
end

return year_2021