local Direction = require '2021.models.Direction'

local day_02 = require('lib.Class'):extend()

function day_02:init()
    self.lines   = lines_from('2021/day_02/day_02-input.txt')
    self.title   = 'Dive!'
    self.content = {}

    M.each(self.lines, function(val, idx)
        dir, count = val:match("([a-z]*) ([0-9]*)")

        M.push(self.content, Direction:new(dir, count))
    end)
end

function day_02:part_1()
    local depth = M.chain(self.content):map(function(v) return v:get_vertical_count() end):sum():value()
    local horiz = M.chain(self.content):map(function(v) return v:get_horizontal_count() end):sum():value()

    return depth * horiz
end

function day_02:part_2()
    local result = 0
    local depth  = 0
    local horiz  = 0
    local aim    = 0
    
    M.each(self.content, function(val, idx)
        if val:get_direction() == "down" then 
            aim   = aim   + val:get_amount()
        elseif val:get_direction() == "up" then
            aim   = aim   - val:get_amount()
        elseif val:get_direction() == "forward" then
            horiz = horiz + val:get_amount()
            depth = depth + (aim * val:get_amount())
        end
    end)

    return depth * horiz
end

return day_02
