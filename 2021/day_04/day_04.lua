local BingoBoard = require '2021.models.BingoBoard'

local day_04 = require('lib.Class'):extend()

function day_04:init()
    self.lines   = lines_from('2021/day_04/day_04-input.txt')
    self.title   = 'Giant Squid'
    self.content = {}
    self.draw_nr = {}

    local bingo_board = nil
    local curr_row    = 1

    M.each(self.lines, function(val, idx)
        if idx == 1 then
            for current_char in val:gmatch('[^,%s]+') do
                M.push(self.draw_nr, current_char)
            end
        else
            if bingo_board == nil or curr_row == 6 then
                M.push(self.content, bingo_board)
                bingo_board = BingoBoard:new()
                curr_row = 1
            end

            if val ~= nil and val ~= "" then
                bingo_board:add_row(val, curr_row)
                curr_row = curr_row + 1
            end
        end
    end)
end

function day_04:part_1()
    local result = nil

    M.each(self.draw_nr, function(val, idx)
        M.each(self.content, function(vl) 
            if result == nil then
                local match = vl:add_draw(val)
                if match ~= nil then 
                    result = match
                end
            end
        end)
    end)

    return result
end

function day_04:part_2()
    local result = nil
    local count_to_match =  M.count(self.content) - 1

    M.each(self.content, function(vl)
        vl:set_draws({})
    end)
    
    M.each(self.draw_nr, function(val, idx)
        M.each(self.content, function(vl) 
            if result == nil and vl:is_complete() ~= true then
                local match = vl:add_draw(val)
                
                if match ~= nil then
                    if M.chain(self.content):select(function(v) return v:is_complete() end):count():value() == count_to_match then
                        result = match
                    else
                        vl:set_complete()
                    end
                end
            end
        end)
    end)

    return result
end

return day_04
