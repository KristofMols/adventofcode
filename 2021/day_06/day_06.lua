local day_06 = require('lib.Class'):extend()

function day_06:init()
    self.lines   = lines_from('2021/day_06/day_06-input.txt')
    self.title   = 'Lanternfish'
    self.content = M.zeros(9)

    self.OFFSET       = 1
    self.RESET_STEP   = 6 + self.OFFSET
    self.INITIAL_STEP = 8 + self.OFFSET

    M.each(self.lines, function(val, idx)
        for current_char in val:gmatch('[^,%s]+') do
            local curr_num         = tonumber(current_char)
            self.content[curr_num] = self.content[curr_num] + 1
        end
    end)
end

function day_06:part_1()
    local temp = M.clone(self.content)

    for idx = self.OFFSET, 80 - self.OFFSET do
        local zero_gen = M.shift(temp)
        M.push(temp, zero_gen)
        temp[self.RESET_STEP] = temp[self.RESET_STEP] + zero_gen
    end

    return M.sum(temp)
end

function day_06:part_2()
    local temp = M.clone(self.content)
    
    for idx = self.OFFSET, 256 - self.OFFSET do
        local zero_gen = M.shift(temp)
        M.push(temp, zero_gen)
        temp[self.RESET_STEP] = temp[self.RESET_STEP] + zero_gen
    end

    return M.sum(temp)
end

return day_06
