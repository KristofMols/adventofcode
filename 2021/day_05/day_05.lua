local Coordinate = require '2021.models.Coordinate'

local day_05 = require('lib.Class'):extend()

function day_05:init()
    self.lines   = lines_from('2021/day_05/day_05-input.txt')
    self.title   = 'Hydrothermal Venture'
    self.content = {}

    M.each(self.lines, function(val, idx)
        M.push(self.content, Coordinate:new(val))
    end)
end

function day_05:part_1() -- not working yet
    local result = {}

    M.each(self.content, function(val, idx)
        M.each(val:get_coord_list(false), function(itm)
            if result[itm] == nil then result[itm] = 0 end
            result[itm] = result[itm] + 1
        end)
    end)

    return M.chain(result)
            :select(function(k) return k > 1 end)
            :count()
            :value()
end

function day_05:part_2() -- not working yet
    local result = {}

    M.each(self.content, function(val, idx)
        M.each(val:get_coord_list(false), function(itm)
            if result[itm] == nil then result[itm] = 0 end
            result[itm] = result[itm] + 1
        end)
    end)

    return M.chain(result)
            :select(function(k) return k > 1 end)
            :count()
            :value()
end

return day_05
