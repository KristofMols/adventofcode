require 'common.util'

local year_2022 = require('lib.Class'):extend()

function year_2022:init()
end

function year_2022:print_splash_screen()
    color_print({
        '%{dim white}@###@##@@#@#@@@@###@@@@#@@@@@@##@@#@#@@@@@@@@@@@@',
        '%{bright white} 15',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}@@@@@@@@@##@#@##@@@#@@@@#@#@@@#@@#@@@@@@#@#@#@@@@',
        '%{bright white} 14',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}@@#@@@#@###@@#@@@@@@@@@#@@@@@#@@@@@@#@@@@@@#@@@@@',
        '%{bright white} 13',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}@#@@##@#@@#@@@@##@@@@###@@@@@#@@@@@@@#@##@##@@@@#',
        '%{bright white} 12',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}#@@@#@@@@@@###@##@@#@@@@#@@@@#@@#@#@@@@@@@@#@@@@#',
        '%{bright white} 11',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}@#@@##@@@@#@@@###@#@@@#@#@@#@#@#@###@@#@@####@@@@',
        '%{bright white} 10',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}#@@#@@@#@#@###@#@@#@#@#@@#@@@@@@@@#@@@@#@@#@##@#|',
        '%{bright white}  9',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}@@@@@@@@@##@#@#@@@##@@@##@@#@#@@@@@@#@@@@##@##@@#',
        '%{bright white}  8',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}##@@#@@@@#@##@@#@@@#@@@@##@@@@@@#@@##@@@@@##@@#@@',
        '%{bright white}  7',
        '%{dim white} **\n'
    })
    color_print({
        '%{dim white}@@##@#@@@@@#.~~.@@@@@@@..@@@@@@@###@@@#@@#@##@@#@',
        '%{bright white}  6',
        '%{bright yellow} **\n'
    })
    color_print({
        '%{dim white}#@#@@####@@@@.~~.@#./\\.\'@#@@@@@@##@##@@@@@#@##@@@',
        '%{bright white}  5',
        '%{bright yellow} **\n'
    })
    color_print({
        '%{dim white}@@@@@@@#@#@.\' ~  \'./\\\'./\\\' .@@@@@#@@@##@@@@@@@@@@',
        '%{bright white}  4',
        '%{bright yellow} **\n'
    })
    color_print({
        '%{dim white}@@@#@#@@#_/ ~   ~  \\ \' \'. \'.\'.@@@@@##@##@@##@#@@#',
        '%{bright white}  3',
        '%{bright yellow} **\n'
    })
    color_print({
        '%{dim white}-~------\'    ~    ~ \'--~-----~-~----___________--',
        '%{bright white}  2',
        '%{bright yellow} **\n'
    })
    color_print({
        '%{dim white}  ~    ~  ~      ~     ~ ~   ~     ~  ~  ~   ~   ',
        '%{bright white}  1',
        '%{bright yellow} **\n'
    })
    io.write('\n')
end

return year_2022