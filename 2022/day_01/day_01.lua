local day_01 = require('lib.Class'):extend()

function day_01:init()
    self.lines   = lines_from('2022/day_01/day_01-input.txt')
    self.title   = 'Calorie Counting'
    self.content = {}
    self.count   = 0

    M.each(self.lines, function(val, idx)
        if val == "" then
            M.push(self.content, self.count)
            self.count = 0
        else
            self.count = self.count + tonumber(val)
        end
    end)
end

function day_01:part_1()
    return M.max(self.content)
end

function day_01:part_2()
    return M.chain(self.content)
        :sort()
        :last(3)
        :sum()
        :value()
end

return day_01
