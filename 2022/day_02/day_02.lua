local Fight = require '2022.models.Fight'

local day_02 = require('lib.Class'):extend()

function day_02:init()
    self.lines   = lines_from('2022/day_02/day_02-input.txt')
    self.title   = 'Rock Paper Scissors'
    self.content = {}

    M.each(self.lines, function(val, idx)
        opp_choice, own_choice = val:match("([ABC]) ([XYZ])")
        
        M.push(self.content, Fight:new(opp_choice, own_choice))
    end)
end

function day_02:part_1()
    local result = 0

    M.each(self.content, function(val, idx)
        result = result + val:get_score_part_one()
    end)

    return result
end

function day_02:part_2()
    local result = 0
    
    M.each(self.content, function(val, idx)
        result = result + val:get_score_part_two()
    end)

    return result
end

return day_02
