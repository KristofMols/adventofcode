local day_06 = require('lib.Class'):extend()

function day_06:init()
    self.lines   = lines_from('2022/day_06/day_06-input.txt')
    self.title   = ''
    self.content = {}

    M.each(split(self.lines[1], '.'), function(v)
        M.push(self.content, v)
    end)
end

function day_06:part_1()
    local result = 0

    for i = 1, M.count(self.content), 1 do
        local sub = M.slice(self.content, i, i + 3)
        local dup = M.duplicates(sub)
        if M.size(dup) == 0 then
            result = i + 3
            break
        end
    end

    return result
end

function day_06:part_2()
    local result = 0
    
    for i = 1, M.count(self.content), 1 do
        local sub = M.slice(self.content, i, i + 13)
        local dup = M.duplicates(sub)
        if M.size(dup) == 0 then
            result = i + 13
            break
        end
    end

    return result
end

return day_06
