local day_05 = require('lib.Class'):extend()
local Stack = require '2022.models.Stack'

function day_05:init()
    self.lines   = lines_from('2022/day_05/day_05-input.txt')
    self.title   = 'Supply Stacks'
    self.content = {}
    self.stack   = Stack:new()

    local stack_lines = {}

    M.each(self.lines, function(val, idx)
        if startswith(val, ' 1') then
            local items = split(val, '.')

            M.each(M.reverse(stack_lines), function(v, i)
                M.each(items, function(iv, ii)
                    if iv ~= ' ' then
                        local stack_val = v:sub(ii, ii)
                        if stack_val ~= ' ' then
                            self.stack:add_box(stack_val, tonumber(iv))
                        end
                    end
                end)
            end)
        elseif not startswith(val, 'move') and val ~= ' ' then
            M.push(stack_lines, val)
        else
            M.push(self.content, val)
        end
    end)
end

function day_05:part_1()
    local box_stacks = M.clone(self.stack.box_stacks)
    
    M.each(self.content, function(val, idx)
        for am, fr, to in val:gmatch("move (%d+) from (%d+) to (%d+)") do
            self.stack:execute_times(tonumber(am), tonumber(fr), tonumber(to))
        end
    end)

    local result = self.stack:get_first_items()

    self.stack.box_stacks = box_stacks

    return result
end

function day_05:part_2()    
    M.each(self.content, function(val, idx)
        for am, fr, to in val:gmatch("move (%d+) from (%d+) to (%d+)") do
            self.stack:execute_amount(tonumber(am), tonumber(fr), tonumber(to))
        end
    end)

    return self.stack:get_first_items()
end

return day_05
