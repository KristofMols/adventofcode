local day_04         = require('lib.Class'):extend()
local CoordinatePair = require '2022.models.CoordinatePair'

function day_04:init()
    self.lines   = lines_from('2022/day_04/day_04-input.txt')
    self.title   = 'Camp Cleanup'
    self.content = {}

    M.each(self.lines, function(val, idx)
        local coords = split(val, "[^,]*")
        local c_1 = split(coords[1], "[^-]*")
        local c_2 = split(coords[2], "[^-]*")
        M.push(self.content, CoordinatePair:new(c_1[1], c_1[2], c_2[1], c_2[2]))
    end)
end

function day_04:part_1()
    local result = 0

    M.each(self.content, function(val, idx)
        if val:is_contained() then 
            result = result + 1 
        end
    end)

    return result
end

function day_04:part_2()
    local result = 0
    
    M.each(self.content, function(val, idx)
        if val:has_overlap() then 
            result = result + 1 
        end
    end)

    return result
end

return day_04
