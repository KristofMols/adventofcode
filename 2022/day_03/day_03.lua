local day_03 = require('lib.Class'):extend()

local lower = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }
local upper = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' }

function day_03:init()
    self.lines   = lines_from('2022/day_03/day_03-input.txt')
    self.title   = 'Rucksack Reorganization'
    self.content = {}

    M.each(self.lines, function(val, idx)
        M.push(self.content, val)
    end)
end

function day_03:part_1()
    local result = 0

    M.each(self.content, function(val, idx)
        part_1 = val:sub(1, val:len() / 2)
        part_2 = val:sub((val:len() / 2) + 1)
        
        intersection = M.unique(M.intersection(split(part_1, '.'), split(part_2, '.')))

        M.each(intersection, function(val)
            lw_fnd = M.find(lower, val) or 0
            up_fnd = M.find(upper, val) or 0

            result = result + lw_fnd + up_fnd

            if up_fnd > 0 then
                result = result + 26
            end 
        end)
    end)

    return result
end

function day_03:part_2()
    local result = 0

    for i = 1, M.count(self.content), 3 do
        intersection = M.unique(M.intersection(split(self.content[i], '.'), split(self.content[i + 1], '.'), split(self.content[i + 2], '.')))

        M.each(intersection, function(val)
            lw_fnd = M.find(lower, val) or 0
            up_fnd = M.find(upper, val) or 0

            result = result + lw_fnd + up_fnd

            if up_fnd > 0 then
                result = result + 26
            end 
        end)
    end

    return result
end

return day_03
