local CoordinatePair = require('lib.Class'):extend()

function CoordinatePair:init(new_x_1, new_x_2, new_y_1, new_y_2)
    self.x_1 = tonumber(new_x_1)
    self.x_2 = tonumber(new_x_2)
    self.y_1 = tonumber(new_y_1)
    self.y_2 = tonumber(new_y_2)
end

function CoordinatePair:is_contained()
    return (self.x_1 <= self.y_1 and self.x_2 >= self.y_2) or (self.y_1 <= self.x_1 and self.y_2 >= self.x_2)
end

function CoordinatePair:has_overlap()
    return not ((self.x_2 < self.y_1) or (self.y_2 < self.x_1))
end

return CoordinatePair
