local Stack = require('lib.Class'):extend()

function Stack:init()
    self.box_stacks = {}
end

function Stack:add_box(val, idx)
    if self.box_stacks[idx] == nil then
        self.box_stacks[idx] = {}
    end

    M.push(self.box_stacks[idx], val)
end

function Stack:execute_times(times, from, to)
    M.times(function(v)
        local v = M.unshift(self.box_stacks[from])
        M.push(self.box_stacks[to], v)
    end, times)
end

function Stack:execute_amount(amount, from, to)
    local boxes = {}
    M.times(function(v)
        local v = M.unshift(self.box_stacks[from])
        M.push(boxes, v)
    end, amount)
    M.each(M.reverse(boxes), function(v)
        M.push(self.box_stacks[to], v)
    end)
end

function Stack:get_first_items()
    local res = ""
    M.each(self.box_stacks, function(v)
        local val = M.unshift(v)
        if val == nil then
            res = res .. ' '
        else
            res = res .. val
        end
    end)
    return res
end

return Stack
