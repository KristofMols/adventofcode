local Fight = require('lib.Class'):extend()

local opp_choices = { 'A', 'B', 'C' }
local own_choices = { 'X', 'Y', 'Z' }

function Fight:init(opp_choice, own_choice)
    self.opp_choice = opp_choice
    self.own_choice = own_choice
end

function Fight:get_score_part_one()
    local own_score = M.detect(own_choices, self.own_choice)
    local opp_score = M.detect(opp_choices, self.opp_choice)

    local score = own_score

    if own_score == opp_score then 
        score = score + 3
    elseif own_score == opp_score + 1 or (own_score == 1 and opp_score == 3) then
        score = score + 6
    end

    return score
end

function Fight:get_score_part_two()
    local own_score = M.detect(own_choices, self.own_choice)
    local opp_score = M.detect(opp_choices, self.opp_choice)

    local score = (own_score - 1) * 3

    if own_score == 1 then
        score = score + (opp_score == 1 and 3 or opp_score - 1)
    elseif own_score == 2 then
        score = score + opp_score
    elseif own_score == 3 then
        score = score + (opp_score == 3 and 1 or opp_score + 1)
    end

    return score
end

return Fight