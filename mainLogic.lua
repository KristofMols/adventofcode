M = require 'lib.Moses'
I = require 'lib.Inspect'

require 'common.util'
require 'common.file'
require 'fileCreator'

local main_file = require('lib.Class'):extend()

function main_file:init(mode, year, day, model)
    self.mode  = mode
    self.year  = year
    self.day   = day
    self.model = model
end

function main_file:get_run_color(final_time)
    if     final_time < 500  then return 'green'
    elseif final_time < 1000 then return 'yellow'
    else                          return 'red'
    end
end

function main_file:get_result_status(year, day, part, part_result)
    local result = nil
    local file_name = year .. '/day_' .. day .. '/day_' .. day .. '-results.txt'
    
    if not file_exists(file_name) then
        result = '%{bright yellow}' .. string.format('%3s', utf8.char(0x003F))
    else
        local lines = lines_from(file_name)
        if lines[part] == tostring(part_result) then
            result = '%{bright green}' .. string.format('%5s', utf8.char(0x221A))
        else
            result = '%{bright red}' .. string.format('%5s', utf8.char(0x2A2F))
        end
    end

    return result
end

function main_file:print_report(day_file, year, day, part)
    local start_time = os.clock()
    local result     = day_file['part_' .. part](day_file)
    local final_time = (os.clock() - start_time) * 1000

    color_print({
        self:get_result_status(year, day, part, result),
        '%{bright white} ' .. string.format('%-15s', result),
        '%{bright '      .. self:get_run_color(final_time) .. '}' .. string.format('%7.2f', final_time) .. ' ms\n',
    })
end

function main_file:run_day(year, day)
    if #day < 2 then day = '0' .. day end
    
    if not file_exists(year .. '/day_' .. day .. '/day_' .. day .. '.lua') then
        color_print({
            '%{bright red}Day ' .. day .. ' Not Found!\n'
        })
    else
        day_file = require(year .. '.day_' .. day .. '.day_' .. day):new()

        color_print({
            '%{bright white}Day ' .. day .. ' : ',
            '%{bright white}' .. day_file.title .. '\n\n'
        })

        self:print_report(day_file, year, day, 1)
        self:print_report(day_file, year, day, 2)
        io.write('\n')
    end
end

function main_file:run_year(year)
    require(year .. '.main_' .. year):new():print_splash_screen()
    
    M.invoke(M.range(25), function(index) 
        self:run_day(year, tostring(index))
    end)
end

function main_file:generate_year(year)
    M.invoke(M.range(25), function(index) 
        self:generate_day(year, string.format("%02d", tostring(index)))
    end)
end

function main_file:generate_day(year, day)
    create_day_files(year, day)
end

function main_file:generate_model(year, model)
    create_model_files(year, model)
end

function main_file:run()
    if self.year == nil then return end

    if     self.model ~= nil then self[self.mode .. '_model'](self, self.year, self.model)
    elseif self.day   == nil then self[self.mode .. '_year'](self, self.year)
    else                          self[self.mode .. '_day'](self, self.year, self.day)
    end
end

return main_file