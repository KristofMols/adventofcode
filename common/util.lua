local colors = require 'lib.ansicolors'

function split(str, pat)
    local tbl = {}
    str:gsub(pat, function(x) tbl[#tbl + 1] = x end)
    return tbl
end

function trim(str)
    return str:match "^%s*(.-)%s*$"
end

function startswith(text, prefix)
    return text:find(prefix, 1, true) == 1
end

function split_str(str, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}
    for str in string.gmatch(str, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end


function color_print(str)
    local new_str = ''

    M.each(str, function(item)
        new_str = new_str .. '%{reset}' .. item
    end)

    io.write(colors(new_str))
end