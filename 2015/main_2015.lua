require 'common.util'

local year_2015 = require('lib.Class'):extend()

function year_2015:init()
end

function year_2015:print_splash_screen()

    color_print({
        '                        *                           ',
        '%{bright white} 25',
        '%{bright white} **\n'
    })
    color_print({
        '                       >o<                          ',
        '%{bright white} 24',
        '%{bright white} **\n'
    })
    color_print({
        '                      >>O<<                         ',
        '%{bright white} 23',
        '%{bright white} **\n'
    })
    color_print({
        '                     >*<<O<<                        ',
        '%{bright white} 22',
        '%{bright white} **\n'
    })
    color_print({
        '                    >O>@>>o<<                       ',
        '%{bright white} 21',
        '%{bright white} **\n'
    })
    color_print({
        '                   >O<<O>>*<<<                      ',
        '%{bright white} 20',
        '%{bright white} **\n'
    })
    color_print({
        '                  >@<<<*<<O>>o<                     ',
        '%{bright white} 19',
        '%{bright white} **\n'
    })
    color_print({
        '                 >>@>>O>O<*<<<o<                    ',
        '%{bright white} 18',
        '%{bright white} **\n'
    })
    color_print({
        '                >*<<o>o<<@<<*<<<<                   ',
        '%{bright white} 17',
        '%{bright white} **\n'
    })
    color_print({
        '               >>O>o<@>>>o>>@>O<<<                  ',
        '%{bright white} 16',
        '%{bright white} **\n'
    })
    color_print({
        '              >@<O>>O<<<o<<O>>>*>*<                 ',
        '%{bright white} 15',
        '%{bright white} **\n'
    })
    color_print({
        '             >O<<@>O>>*<<O<<@>o>>>O<                ',
        '%{bright white} 14',
        '%{bright white} **\n'
    })
    color_print({
        '            >@>>*<*<<<O>>*<<*<<@<o>@<               ',
        '%{bright white} 13',
        '%{bright white} **\n'
    })
    color_print({
        '           >>@>o>>>o<@<<o<<<O<o<O<<<@<              ',
        '%{bright white} 12',
        '%{bright white} **\n'
    })
    color_print({
        '          >*<@>o<@>>@<*<<o<<<@>>>*>>@<<             ',
        '%{bright white} 11',
        '%{bright white} **\n'
    })
    color_print({
        '         >>@>>>O>>O<@>o>>>@>*<<<O<<*<<@<            ',
        '%{bright white} 10',
        '%{bright white} **\n'
    })
    color_print({
        '        >*>*<<*>>>@<<@>@<<O<<<*>>>@>>>@<<           ',
        '%{bright white}  9',
        '%{bright white} **\n'
    })
    color_print({
        '       >>@>>@<O<o<O<@>o>>>O>>@<@>>*<<*>>@<          ',
        '%{bright white}  8',
        '%{bright white} **\n'
    })
    color_print({
        '      >O>*>@<<<*>>o<<<*<o>O<o<<<*>*<<<O<<*<         ',
        '%{bright white}  7',
        '%{bright white} **\n'
    })
    color_print({
        '     >>*>@<<<o>O>@<*<@>>*<<<O>>>O>O<*<@<o<@<        ',
        '%{bright white}  6',
        '%{bright white} **\n'
    })
    color_print({
        '    >*<<O<<*<<o>>>@<O>>>@<@<<<o>@>>>O<<*<<O<<       ',
        '%{bright white}  5',
        '%{bright white} **\n'
    })
    color_print({
        '   >>o<<@<<*<<O>O>>O>>*>*>*<*>*<<*<<<o<<*<o>O<      ',
        '%{bright white}  4',
        '%{bright white} **\n'
    })
    color_print({
        '  >>*>>o>>>@<*<@<<<*>>O<<<*>@<@>>>*>>O>>*<<@<<<     ',
        '%{bright white}  3',
        '%{bright white} **\n'
    })
    color_print({
        ' >>@>*<*<<*>O>>@>O<<o<<<o>>o<@>@>>@>>>@<<<@>>@<<    ',
        '%{bright white}  2',
        '%{bright yellow} **\n'
    })
    color_print({
        '>>o>>>@>o<<<O<*>>>*<*<<<o<@<O<<<*>O>>>o<@<<o<o>@<   ',
        '%{bright white}  1',
        '%{bright yellow} **\n'
    })
    color_print({
        '                      |   |                         \n'
    })
    color_print({
        '                      |   |                         \n'
    })
    color_print({
        '           _  _ __ ___|___|___ __ _  _              \n'
    })
    io.write('\n')
end

return year_2015
