local day_01 = require('lib.Class'):extend()

function day_01:init()
    self.lines   = lines_from('2015/day_01/day_01-input.txt')
    self.items   = string.gmatch(lines[1], ".")
    self.title   = 'Not Quite Lisp'
    self.content = {}

    for item in self.items do
        M.push(self.content, item)
    end
end

function day_01:part_1()
    local result = 0

    M.each(self.content, function(val, idx)
        if val == '(' then
            result = result + 1
        elseif val == ')' then
            result = result - 1
        end
    end)

    return result
end

function day_01:part_2()
    local result = 0
    local floor  = 0
    
    for _, val in pairs(self.content) do
        if val == '(' then
            floor = floor + 1
        elseif val == ')' then
            floor = floor - 1
        end
    
        result = result + 1
    
        if floor == -1 then
            break
        end
    end

    return result
end

return day_01
