local Present = require '2015.models.Present'

local day_02 = require('lib.Class'):extend()

function day_02:init()
    self.lines   = lines_from('2015/day_02/day_02-input.txt')
    self.title   = 'I Was Told There Would Be No Math'
    self.content = {}

    M.each(self.lines, function(val, idx)
        M.push(self.content, Present:new(val))
    end)
end

function day_02:part_1()
    local result = 0

    M.each(self.content, function(val, idx)
        result = result + math.floor(val:calculate_wrapping())
    end)

    return result
end

function day_02:part_2()
    local total_ribbon = 0
    local total_bow    = 0

    M.each(self.content, function(val, idx)
        total_ribbon = total_ribbon + math.floor(val:calculate_ribbon())
        total_bow    = total_bow    + math.floor(val:calculate_bow())
    end)

    return total_ribbon + total_bow
end

return day_02
