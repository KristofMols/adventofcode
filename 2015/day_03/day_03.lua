local day_03 = require('lib.Class'):extend()

function day_03:init()
    self.lines     = lines_from('2015/day_03/day_03-input.txt')
    self.title     = 'Perfectly Spherical Houses in a Vacuum'
    self.current_x = 0
    self.current_y = 0
    self.content   = {}

    for current_char in string.gmatch(self.lines[1], ".") do
        self.current_x, self.current_y = self.move_character(current_char, self.current_x, self.current_y)
    end    
end

function day_03:part_1()
    local result    = 0
    
    self.add_or_update_location(self.current_x, self.current_y)

    M.each(self.content, function(val, idx)
        if val:get_amount() > 0 then result = result + 1 end
    end)

    return result
end

function day_03:part_2()
    local result = 0
    
    M.each(self.content, function(val, idx)
        
    end)

    return result
end

function day_03:add_or_update_location(cur_x, cur_y)
    local location = self.content[cur_x .. '|' .. cur_y]
    if location == nil then
        self.content[cur_x .. '|' .. cur_y] = Coordinate:new(cur_x, cur_y)
    else
        self.content:add_amount(1)
    end
end

function day_03:move_character(current_char, cur_x, cur_y) 
    if current_char == '<' then
        cur_x = cur_x - 1
    elseif current_char == '>' then
        cur_x = cur_x + 1
    elseif current_char == '^' then
        cur_y = cur_y + 1
    elseif current_char == 'v' then
        cur_y = cur_y - 1
    end

    self.add_or_update_location(cur_x, cur_y)

    return cur_x, cur_y
end

return day_03
