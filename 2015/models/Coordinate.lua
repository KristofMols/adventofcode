local Coordinate = require('lib.Class'):extend()

function Coordinate:init(new_x, new_y)
    self.x      = new_x
    self.y      = new_y
    self.amount = 1
end

function Coordinate:get_x() 
    return self.x 
end

function Coordinate:get_y() 
    return self.y 
end

function Coordinate:get_amount() 
    return self.amount 
end

function Coordinate:add_amount(new_amount) 
    self.amount = self.amount + new_amount 
end

return Coordinate
