local Present = require('lib.Class'):extend()

function Present:init(dimensions)
    local dim = split(dimensions, "[^x]*")

    self.l = tonumber(dim[1])
    self.w = tonumber(dim[2])
    self.h = tonumber(dim[3])
end

function Present:get_data() return self.l, self.w, self.h end

function Present:calculate_wrapping()
    local side_a = self.l * self.w
    local side_b = self.w * self.h
    local side_c = self.h * self.l

    local smallest = math.min(side_a, side_b, side_c)

    return (2 * side_a + 2 * side_b + 2 * side_c) + smallest
end

local function compare(a, b) return a < b end

function Present:calculate_ribbon()
    local tab = {self.l, self.w, self.h}
    table.sort(tab, compare)

    return tab[1] * 2 + tab[2] * 2
end

function Present:calculate_bow() return self.l * self.w * self.h end

return Present
