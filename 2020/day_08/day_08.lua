local day_08 = require('lib.Class'):extend()

function day_08:init()
    self.lines   = lines_from('2020/day_08/day_08-input.txt')
    self.title   = 'Handheld Halting'
end

function day_08:part_1()
    local current_index  = 1
    local current_result = 0
    local previous_visit = {}

    while not previous_visit[current_index] do
        previous_visit[current_index] = true
        instruction = self.lines[current_index]
        operation, argument = instruction:match("(%a%a%a)%s([%+%-]%d+)")

        if operation == "acc" then
            current_result = current_result + tonumber(argument)
            current_index = current_index + 1
        elseif operation == "jmp" then
            current_index = current_index + tonumber(argument)
        elseif operation == "nop" then
            current_index = current_index + 1
        end
    
    end

    return current_result
end

function day_08:part_2()
    local index_to_replace = 1
    local program_succesfull = false
    local current_result = nil

    while not program_succesfull do
        local current_index  = 1
        local previous_visit = {}
        current_result = 0

        while not previous_visit[current_index] and current_index <= M.count(self.lines) do
            previous_visit[current_index] = true
            instruction = self.lines[current_index]
            operation, argument = instruction:match("(%a%a%a)%s([%+%-]%d+)")

            if index_to_replace == current_index then 
                if     operation == "jmp" then operation = "nop"
                elseif operation == "nop" then operation = "jmp"
                end 
            end
            
            if operation == "acc" then
                current_result = current_result + tonumber(argument)
                current_index = current_index + 1
            elseif operation == "jmp" then
                current_index = current_index + tonumber(argument)
            elseif operation == "nop" then
                current_index = current_index + 1
            end
        end

        if current_index == M.count(self.lines) + 1 then program_succesfull = true end
        index_to_replace = index_to_replace + 1
    end

    return current_result
end

return day_08
