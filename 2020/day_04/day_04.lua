local Passport = require '2020.models.Passport'

local day_04 = require('lib.Class'):extend()

function day_04:init()
    self.lines = lines_from('2020/day_04/day_04-input.txt')
    self.title = 'Passport Processing'

    self.passports = {}

    local last_pass = Passport:new()
    M.push(self.passports, last_pass)
    
    M.each(lines, function(val, idx)
        if val == '' then 
            last_pass = Passport:new() 
            M.push(self.passports, last_pass)
        else
            for str in val:gmatch("([^" .. '%s' .. "]+)") do
                last_pass:add_field(str)
            end
        end
    end)
end

function day_04:part_1()
    local result = 0

    M.each(self.passports, function(passport) 
        if passport:is_valid() then
            result = result + 1
        end
    end)

    return result
end

function day_04:part_2()
    local result = 0
    
    M.each(self.passports, function(passport) 
        if passport:is_valid(true) then
            result = result + 1
        end
    end)

    return result - 1 -- why - 1?
end

return day_04