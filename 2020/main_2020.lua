require 'common.util'

local year_2020 = require('lib.Class'):extend()

function year_2020:init()
end

function year_2020:print_splash_screen()
    color_print({
        '              ..........|..........               ',
        '%{bright white}  1',
        '%{bright yellow} **\n'
    })
    color_print({
        '   .....\'\'\'\'\'\' .\'  -  -  \\- .\'\'~ ~ \'\'\'\'\'\'.....    ',
        '%{bright white}  2',
        '%{bright yellow} **\n'
    })
    color_print({
        '\'\'\' ~ ~ ~ ~  ~ \'.\'. -   - \\ -\'\':  ~ ~   ~  ~  \'\'\' ',
        '%{bright white}  3',
        '%{bright yellow} **\n'
    })
    color_print({
        ' ~   ~  ~   ~ ~  ~ \'\'..\'\'\'_[].\'  ~    ~   ~ ~  ~  ',
        '%{bright white}  4',
        '%{bright yellow} **\n'
    })
    color_print({
        '~   ~ ~  .\'. ~  ~  ~ ____/ \'\'  ~  ~  ~  ~  ~    ~ ',
        '%{bright white}  5',
        '%{bright yellow} **\n'
    })
    color_print({
        '  ~    ~ \'\'  .._____/ ~   ~  ~  ~    _ ~ _   O>   ',
        '%{bright white}  6',
        '%{bright yellow} **\n'
    })
    color_print({
        ' ~  ~ ~   ~ :  \'.   ~   ~      ~  \\ / \\ / \\ /  ~  ',
        '%{bright white}  7',
        '%{bright yellow} **\n'
    })
    color_print({
        '       ~     \'.\' ~        ~  ~   ~  ~      ~    ~ ',
        '%{bright white}  8',
        '%{dim yellow} **\n'
    })
    color_print({
        ' ~   ~      ~      ~   ~        ~      ~      ~   ',
        '%{bright white} 17',
        '%{dim yellow} **\n'
    })
    color_print({
        '       ~       ~        .\'\'\'\'..    ~       ~    . ',
        '%{bright white}  9',
        '%{dim yellow} **\n'
    })
    color_print({
        '    ~       ~       ~   .\'^ , .\'      ~  ~  ..\'\'  ',
        '%{bright white} 16',
        '%{dim yellow} **\n'
    })
    color_print({
        '...     ~      ~        :^ , :   ~       :\'\'  , ^ ',
        '%{bright white} 18',
        '%{dim yellow} **\n'
    })
    color_print({
        '###: ...    ~      ~     \'..\'          ~ \'. ,     ',
        '%{bright white} 15',
        '%{dim yellow} **\n'
    })
    color_print({
        '.\'\' .\'##\'.             ~       ~   ~      :  , ^  ',
        '%{bright white} 19',
        '%{dim yellow} **\n'
    })
    color_print({
        '\'...\'#####\'.  ~    ~      ~            .\'\' ,  ^ ^ ',
        '%{bright white} 10',
        '%{dim yellow} **\n'
    })
    color_print({
        '##### ,###.\'               .       ~   \'.    , ^  ',
        '%{bright white} 14',
        '%{dim yellow} **\n'
    })
    color_print({
        '#,      , \'.         ~  .\'\',:  ~         \'..  , ^ ',
        '%{bright white} 20',
        '%{dim yellow} **\n'
    })
    color_print({
        '   ~ ~   , ,\'.   ~     :, ..\'           ~   \'\'... ',
        '%{bright white} 11',
        '%{dim yellow} **\n'
    })
    color_print({
        '    ~ ~     , :         \'\'     ~                  ',
        '%{bright white} 13',
        '%{dim yellow} **\n'
    })
    color_print({
        ' ~ ~ ~ ~ ~ , ,:     ~     ~          ~      ~     ',
        '%{bright white} 21',
        '%{dim yellow} **\n'
    })
    color_print({
        '                                                  ',
        '%{bright white} 12',
        '%{dim yellow} **\n'
    })
    color_print({
        ' ~ ~ ~    ..\'   ~               ~         ~    ~  ',
        '%{bright white} 22',
        '%{dim yellow} **\n'
    })
    color_print({
        '  .....\'\'\'           ~     ~                      ',
        '%{bright white} 23',
        '%{dim yellow} **\n'
    })
    color_print({
        '\'\'         ~                 .\'..   .\'\'..       ~ ',
        '%{bright white} 24',
        '%{dim yellow} **\n'
    })
    color_print({
        '     ~          ~        ~    \'.\'  :    .\'  ~     ',
        '%{bright white} 25',
        '%{dim yellow} **\n'
    })
    io.write('\n')
end

return year_2020