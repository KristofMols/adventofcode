local day_01 = require('lib.Class'):extend()

function day_01:init()
    self.lines = lines_from('2020/day_01/day_01-input.txt')
    self.title = 'Report Repair'
    self.content = {}

    M.each(self.lines, function(val, idx)
        M.push(self.content, tonumber(val))
    end)
end

local function find_sum_of_2(slice, sum)
    local l = 1
    local r = M.count(slice)

    while l < r do
        if slice[l] + slice[r] == sum    then return math.floor(slice[l] * slice[r])
        elseif slice[l] + slice[r] < sum then l = l + 1
        else                                  r = r - 1
        end
    end

    return -1
end

local function find_sum_of_3(slice, sum)
    local l = nil
    local r = nil

    for i = 1, M.count(slice), 1 do
        l = i + 1
        r = M.count(slice)

        while l < r do
            if slice[i] + slice[l] + slice[r] == sum then return math.floor(slice[i] * slice[l] * slice[r])
            elseif slice[i] + slice[l] + slice[r] < sum then l = l + 1
            else                                  r = r - 1
            end
        end
    end

    return -1
end

function day_01:part_1()
    return find_sum_of_2(M.sort(self.content), 2020)
end

function day_01:part_2()
    return find_sum_of_3(M.sort(self.content), 2020)
end

return day_01