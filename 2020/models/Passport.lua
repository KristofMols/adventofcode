local Passport = require('lib.Class'):extend()

function Passport:init()
    self.fields = {}
end

function Passport:add_field(field)
    M.push(self.fields, field)
end

function Passport:has_field(find_field)
    return M.countf(self.fields, function(field) 
        return field:match('^' .. find_field) ~= nil 
    end) > 0
end

function Passport:validate_byr(val) 
    return 
        val:match('%d%d%d%d') ~= nil and 
        tonumber(val) >= 1920 and 
        tonumber(val) <= 2002 
end

function Passport:validate_iyr(val) 
    return 
        val:match('%d%d%d%d') ~= nil and 
        tonumber(val) >= 2010 and 
        tonumber(val) <= 2020 
end

function Passport:validate_eyr(val) 
    return 
        val:match('%d%d%d%d') ~= nil and 
        tonumber(val) >= 2020 and 
        tonumber(val) <= 2030 
end

function Passport:validate_hgt(val)
    val_v, val_k = val:match('(%d+)(%a%a)')
    
    return val_v ~= nil and val_k ~= nil and (
        (val_k == 'in' and tonumber(val_v) >= 59  and tonumber(val_v) <= 76  ) or 
        (val_k == 'cm' and tonumber(val_v) >= 150 and tonumber(val_v) <= 193 ) 
    )
end

function Passport:validate_hcl(val) return val:match('#%x%x%x%x%x%x') ~= nil end

function Passport:validate_ecl(val) 
    return 
        val ~= nil and (
            val == 'amb' or 
            val == 'blu' or 
            val == 'brn' or 
            val == 'gry' or 
            val == 'grn' or 
            val == 'hzl' or 
            val == 'oth' 
        )
end

function Passport:validate_pid(val) return val:match('%d%d%d%d%d%d%d%d%d') ~= nil end

function Passport:validate_cid(val) return true end

function Passport:get_field_content(field)
    return field:match('(.+):(.+)')
end

function Passport:is_valid_content()
    local valid = true

    M.each(self.fields, function(field)
        key, val = self:get_field_content(field)
        if (key ~= nil and val ~= nil) then 
            if not self['validate_' .. key](self, val) then
                valid = false
            end
        end
    end)
    
    return valid
end

function Passport:is_valid(check_content)
    local valid = self:has_field("byr")
              and self:has_field("iyr")
              and self:has_field("eyr")
              and self:has_field("hgt")
              and self:has_field("hcl")
              and self:has_field("ecl")
              and self:has_field("pid")

    if valid and check_content then
        valid = self:is_valid_content()
    end

    return valid
end

return Passport