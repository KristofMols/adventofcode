local day_07 = require('lib.Class'):extend()

function day_07:init()
    self.lines   = lines_from('2020/day_07/day_07-input.txt')
    self.title   = 'Handy Haversacks'
end

local count = nil
local counted = nil
local content = nil

local function count_containers(bag_type)
    if content[bag_type] then
        M.each(content[bag_type], function(_, container)
            if not counted[container] then
                counted[container] = true
                count_containers(container)
            end
        end)
    end
end

function day_07:part_1()
    content = {}
    counted = {}

    M.each(self.lines, function(line)
        container, contents = line:match("(.+)%sbags%scontain%s(.+)%.")
        for amount, bagtype in contents:gmatch("(%d)%s(%a+%s%a+)%sbag") do
            if not content[bagtype] then content[bagtype] = {} end
            content[bagtype][container] = amount
        end
    end)

    count_containers('shiny gold') 

    return M.count(counted)
end

local function count_bags(bag_type)
    local result = 0

    M.each(content[bag_type], function(count, child)
        result = result + tonumber(count) + tonumber(count) * count_bags(child)
    end)

    return result
end

function day_07:part_2()
    content = {}

    M.each(self.lines, function(line)
        container, contents = line:match("(.+)%sbags%scontain%s(.+)%.")
        content[container] = {}
        
        for amount, bagtype in contents:gmatch("(%d)%s(%a+%s%a+)%sbag") do
            content[container][bagtype] = amount
        end
    end)

    return count_bags('shiny gold') 
end

return day_07
