I = require 'lib.inspect'

local day_06 = require('lib.Class'):extend()

function day_06:init()
    self.lines = lines_from('2020/day_06/day_06-input.txt')
    self.title = 'Custom Customs'

    self.documents = {}

    local last_doc = {}
    M.push(self.documents, last_doc)
    
    M.each(self.lines, function(val, idx)
        if val == '' then 
            last_doc = {} 
            M.push(self.documents, last_doc)
        else
            M.push(last_doc, split(val, '.'))
        end
    end)
end

function day_06:part_1()
    return M.chain(self.documents)
             :map(function(val) return M.count(M.union(table.unpack(val))) end)
             :sum()
             :value()
end

function day_06:part_2()
    return M.chain(self.documents)
             :map(function(val) return M.count(M.intersection(table.unpack(val))) end)
             :sum()
             :value()
end

return day_06