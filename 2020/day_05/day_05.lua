local day_05 = require('lib.Class'):extend()

local MIN_ROW = 0
local MAX_ROW = 127
local MIN_SEAT = 0
local MAX_SEAT = 7

function day_05:init()
    self.lines = lines_from('2020/day_05/day_05-input.txt')
    self.title = 'Binary Boarding'
end

local function calculate_new_pos(cur_pos, char)
    if char == 'F' or char == 'L' then
        cur_pos.max = math.floor((cur_pos.max - cur_pos.min) / 2) + cur_pos.min
    elseif char == 'B' or char == 'R' then
        cur_pos.min = math.ceil((cur_pos.max - cur_pos.min) / 2) + cur_pos.min
    end
end

local function calculate_seat(val)
    local split_list = split(val, '.')
    local current_row = {min = MIN_ROW, max = MAX_ROW}
    local current_seat = {min = MIN_SEAT, max = MAX_SEAT}

    M.each(split_list, function(item)
        if item == 'F' or item == 'B' and current_row.min ~= current_row.max then
            calculate_new_pos(current_row, item)
        elseif item == 'L' or item == 'R' and current_row.min == current_row.max then
            calculate_new_pos(current_seat, item)
        else
            print('ERROR')
        end
    end)

    local current_seat_id = current_row.min * 8 + current_seat.min

    return current_row.min, current_seat.min, current_seat_id
end

function day_05:part_1()
    local result = 0

    M.each(self.lines, function(val, idx)
        local _, _, current_seat_id = calculate_seat(val)

        if result < current_seat_id then result = current_seat_id end
    end)

    return result
end

function day_05:part_2()
    local matrix = {}

    M.invoke(M.range(MAX_ROW + 1), function(row_idx) 
        matrix[row_idx] = {}
        M.invoke(M.range(MAX_SEAT + 1), function(seat_idx) 
            matrix[row_idx][seat_idx] = 0
        end)
    end)

    local result = 0

    M.each(self.lines, function(val, idx)
        local current_row, current_seat, current_seat_id = calculate_seat(val)
        matrix[current_row + 1][current_seat + 1] = current_seat_id
    end)

    M.each(matrix, function(row)
        M.each(row, function(seat, idx)
            if tostring(seat) == '0' and tostring(row[idx + 1]) ~= '0' and tostring(row[idx - 1]) ~= '0' then
                result = row[idx - 1] + 1
            end
        end)
    end)

    return result
end

return day_05
