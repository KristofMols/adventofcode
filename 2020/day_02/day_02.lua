local day_02 = require('lib.Class'):extend()

function day_02:init()
    self.lines = lines_from('2020/day_02/day_02-input.txt')
    self.title = 'Password Philosophy'
end

function day_02:part_1()
    local result = 0

    M.each(self.lines, function(val, idx) 
        local _, _, am1, am2, ltr, str = val:find("(%d*)-(%d*) (%a+): (%a*)")
        local _, count = str:gsub(ltr, "")

        if count <= tonumber(am2) and count >= tonumber(am1) then
            result = result + 1
        end
    end)

    return result
end

function day_02:part_2()
    local result = 0
    
    M.each(self.lines, function(val, idx) 
        local _, _, am1, am2, ltr, str = val:find("(%d*)-(%d*) (%a+): (%a*)")
        local idx1, _ = str:sub(am1, am1) 
        local idx2, _ = str:sub(am2, am2) 
    
        if (idx1 == ltr and idx2 ~= ltr) or (idx2 == ltr and idx1 ~= ltr) then
            result = result + 1
        end
    end)

    return result
end

return day_02