local day_03 = require('lib.Class'):extend()

function day_03:init()
    self.lines = lines_from('2020/day_03/day_03-input.txt')
    self.title = 'Toboggan Trajectory'

    self.input_map = {}

    M.each(self.lines, function(val, idx)
        self.input_map[idx] = {}
        for i = 1, #val do
            self.input_map[idx][i] = val:sub(i, i)
        end
    end)
end

local function calculate_trees(input_map, down, right)
    local x_pos = 1
    local result = 0

    for i = 1, #input_map, down do
        if input_map[i][x_pos] == '#' then result = result + 1 end
        x_pos = x_pos + right
        if x_pos > #input_map[i] then x_pos = x_pos - #input_map[i] end
    end

    return result
end

function day_03:part_1()
    return calculate_trees(self.input_map, 1, 3)
end

function day_03:part_2()
    local result = 0
    
    result = calculate_trees(self.input_map, 1, 1)
    result = result * calculate_trees(self.input_map, 1, 3)
    result = result * calculate_trees(self.input_map, 1, 5)
    result = result * calculate_trees(self.input_map, 1, 7)
    result = result * calculate_trees(self.input_map, 2, 1)
    
    return result
end

return day_03