local day_09 = require('lib.Class'):extend()

function day_09:init()
    self.lines   = lines_from('2020/day_09/day_09-input.txt')
    self.title   = 'Encoding Error'
    self.content = {}
    self.preamble_size = 25

    M.each(self.lines, function(val, idx)
        M.push(self.content, tonumber(val))
    end)
end

local function find_sum(slice, sum)
    l = 1
    r = M.count(slice)

    while sum <= slice[r] do
        r = r - 1
    end

    while l < r do
        if slice[l] + slice[r] == sum    then return true
        elseif slice[l] + slice[r] < sum then l = l + 1
        else                                  r = r - 1
        end
    end

    return false
end

function day_09:part_1()
    local result = 0

    M.each(self.content, function(val, idx)
        if idx >= self.preamble_size + 1 and result == 0 then
            local sum_found = find_sum(
                M.chain(self.content)
                    :slice(idx - self.preamble_size, idx - 1)
                    :sort()
                    :value(),
                val
            )

            if not sum_found and result == 0 then 
                result = val 
            end
        end
    end)

    return result
end

function day_09:part_2()
    local result = 0
    local sum_to_find = self:part_1()

    for i = 1, M.count(self.content), 1 do
        local current_sum  = 0
        local current_idx  = i + 1
        local used_numbers = {}
        
        M.push(used_numbers, self.content[i])

        while current_sum < sum_to_find and result == 0 do
            current_sum = current_sum + self.content[current_idx]
            M.push(used_numbers, self.content[current_idx])
            current_idx = current_idx + 1
        end

        if current_sum == sum_to_find then 
            M.sort(used_numbers)
            result = M.shift(used_numbers) + M.unshift(used_numbers) 
        end
    end

    return result
end

return day_09
