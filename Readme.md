# Introduction

# Commands

## Generate

`lua main.lua generate --year 2020 --day 07`  
`lua main.lua generate --year 2020`

## Run

`lua main.lua run --year 2020 --day 07`  
`lua main.lua run --year 2020`
