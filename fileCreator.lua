require 'common.file'

local function create_dir(year, day)
    if (os.getenv("oS") or ""):match("^Windows") then
       os.execute('mkdir "' .. year .. '/day_' .. day .. '" 1>nul: 2>&1')
    else
       os.execute('mkdir -p ' .. year .. '/day_' .. day .. ' 2>/dev/null')
    end
end

local function create_file(file_path)
    if file_exists(file_path) then 
        print('file already exists: ' .. file_path)
        return 
    end

    file = io.open(file_path, 'w')
    if not file then 
        print('Can\'t create file: ' .. file_path) 
        return 
    end

    return file
end

local function create_day_input(year, day)
    file = create_file(year .. '/day_' .. day .. '/day_' .. day .. '-input.txt')
    
    file:write("")
    file:close()
end

local function create_day_results(year, day)
    file = create_file(year .. '/day_' .. day .. '/day_' .. day .. '-results.txt')
    
    file:write("-1\n")
    file:write("-1\n")
    file:close()
end

local function create_day_logic(year, day)
    file = create_file(year .. '/day_' .. day .. '/day_' .. day .. '.lua')

    file:write("local day_" .. day .. " = require('lib.Class'):extend()\n")
    file:write("\n")
    file:write("function day_" .. day .. ":init()\n")
    file:write("    self.lines   = lines_from('" .. year .. "/day_" .. day .. "/day_" .. day .. "-input.txt')\n")
    file:write("    self.title   = ''\n")
    file:write("    self.content = {}\n\n")
    file:write("    M.each(self.lines, function(val, idx)\n")
    file:write("        \n")
    file:write("    end)\n")
    file:write("end\n")
    file:write("\n")
    file:write("function day_" .. day .. ":part_1()\n")
    file:write("    local result = 0\n")
    file:write("\n")
    file:write("    M.each(self.content, function(val, idx)\n")
    file:write("        \n")
    file:write("    end)\n")
    file:write("\n")
    file:write("    return result\n")
    file:write("end\n")
    file:write("\n")
    file:write("function day_" .. day .. ":part_2()\n")
    file:write("    local result = 0\n")
    file:write("    \n")
    file:write("    M.each(self.content, function(val, idx)\n")
    file:write("        \n")
    file:write("    end)\n")
    file:write("\n")
    file:write("    return result\n")
    file:write("end\n")
    file:write("\n")
    file:write("return day_" .. day .. "\n")

    file:close()
end

function create_day_files(year, day)
    create_dir(year, day)
    create_day_input(year, day)
    create_day_results(year, day)
    create_day_logic(year, day)
end

function create_model_files(year, model)
    file = create_file(year .. '/models' .. '/' .. model .. '.lua')
    
    file:write("local " .. model .. " = require('lib.Class'):extend()\n")
    file:write("\n")
    file:write("function " .. model .. ":init()\n")
    file:write("end\n")
    file:write("\n")
    file:write("return " .. model .. "\n")

    file:close()
end
