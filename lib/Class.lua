local BaseClass = {}

function BaseClass:new(...)
    local t = setmetatable({}, self)
    t:init(...)
    return t
end

function BaseClass:extend(class_name, t)
    t            = t or {}
    t.__index    = t
    t.super      = self
    t.class_name = class_name

    return setmetatable(t, {__call = self.new, __index = self})
end

function BaseClass:instance_of(instance_to_check)
    local mt = getmetatable(self)
    while mt do
        if mt.__index.class_name == instance_to_check.__index.class_name then
            return true
        end
        mt = getmetatable(mt)
    end
    return false
  end

function BaseClass:init()
end

return BaseClass
